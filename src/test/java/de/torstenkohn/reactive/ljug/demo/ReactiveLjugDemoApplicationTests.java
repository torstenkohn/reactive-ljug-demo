package de.torstenkohn.reactive.ljug.demo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;

@RunWith(SpringRunner.class)
@WebFluxTest(controllers = {PizzaRestController.class, PizzaService.class})
public class ReactiveLjugDemoApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private PizzaRepository repository;

    private Pizza pizza;

    @Before
    public void setUp() throws Exception {
        this.pizza = new Pizza("db7cdccf-3f79-4d6c-a94c-693841d006a1", "Test Pizza");
        Mockito.when(this.repository.findAll()).thenReturn(Flux.just(this.pizza));
        Mockito.when(this.repository.findById(this.pizza.getId())).thenReturn(Mono.just(this.pizza));
    }

    @Test
    public void testFindAll() {
        StepVerifier.create(
                webTestClient
                        .get()
                        .uri("/api/pizzas")
                        .exchange()
                        .expectStatus().isOk()
                        .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                        .returnResult(Pizza.class)
                        .getResponseBody()
                        .take(1))
                .thenAwait(Duration.ofSeconds(10))
                .expectNext(this.pizza)
                .verifyComplete();
    }

    @Test
    public void testEventsTake5() {
        StepVerifier.create(
                webTestClient
                        .get()
                        .uri("/api/pizzas/{pizzaId}/orders", pizza.getId())
                        .exchange()
                        .expectStatus().isOk()
                        .expectHeader().contentType(MediaType.parseMediaType("text/event-stream;charset=UTF-8"))
                        .returnResult(Pizza.class)
                        .getResponseBody()
                        .take(5))
                .expectNextCount(5)
                .verifyComplete();
    }
}
