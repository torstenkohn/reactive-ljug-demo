package de.torstenkohn.reactive.ljug.demo;

import org.junit.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;

public class Playground {


    @Test
    public void createMonoAndSubscribe() {
        // Not a real thing, only to show something
        Mono.just("Hello World")
                .map(value -> value.split("(?!^)"))
                .flatMapMany(Flux::fromArray)
                .filter(character -> !"L".equalsIgnoreCase(character))
                .collectList()
                .map(list -> String.join("", list))
                .subscribe(System.out::println);
    }

    @Test
    public void createFluxAndSubscribe() {
        Flux<String> abc = Flux
                .just("A", "B", "C");

        Flux<String> cba = Flux
                .fromIterable(
                        Arrays.asList("c", "b", "a"));

        abc.zipWith(cba).subscribe(System.out::println);
    }

    @Test
    public void ownSubscriber() {
        Flux.just("Dies ist ein Test mit ein paar Zeichen".split("(?!^)"))
                //.doOnNext(System.out::println)
                .subscribe(new Subscriber<String>() {
                    private Subscription subscription;
                    private int amount;
                    private String cache = "";
                    @Override
                    public void onSubscribe(Subscription subscription) {
                        this.subscription = subscription;
                        this.subscription.request(3);
                    }

                    @Override
                    public void onNext(String s) {
                        amount++;
                        this.cache = this.cache.concat(s);
                        if(amount % 3 == 0){
                            this.subscription.request(3);
                            System.out.println(this.cache);
                            this.cache = "";
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
